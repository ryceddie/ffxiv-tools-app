package zc.games.ffxiv.tools.app;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.MediaType;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import zc.games.ffxiv.tools.calculator.CraftingCalculator;
import zc.games.ffxiv.tools.calculator.GearCalculator;
import zc.games.ffxiv.tools.data.BaseData;
import zc.games.ffxiv.tools.data.ItemParser;
import zc.games.ffxiv.tools.data.RecipeParserV5;
import zc.games.ffxiv.tools.data.init.BaseDataInitialization;
import zc.games.ffxiv.tools.data.repository.FreeCompanyRecipeRepository;
import zc.games.ffxiv.tools.data.repository.ItemRepository;
import zc.games.ffxiv.tools.data.repository.PlayerCharacterDataRepository;
import zc.games.ffxiv.tools.data.repository.PlayerCharacterRepository;
import zc.games.ffxiv.tools.data.repository.RecipeRepository;
import zc.games.ffxiv.tools.data.service.ActorService;
import zc.games.ffxiv.tools.data.service.InventoryService;
import zc.games.ffxiv.tools.data.service.ItemRecipeResolver;
import zc.games.ffxiv.tools.data.service.ItemService;
import zc.games.ffxiv.tools.data.service.PlayerCharacterService;
import zc.games.ffxiv.tools.web.ActorController;
import zc.games.ffxiv.tools.web.CraftingController;
import zc.games.ffxiv.tools.web.EquipmentUpgradeController;
import zc.games.ffxiv.tools.web.InventoryController;
import zc.games.ffxiv.tools.web.ItemController;
import zc.games.ffxiv.tools.web.PlayerCharacterController;

@SpringBootApplication
@RestController
public class App {
	
	public static void main(String...args) {
		SpringApplication.run(App.class, args);
	}
	
	@GetMapping(path = "/api/whoami", produces = MediaType.TEXT_PLAIN_VALUE)
	public String selfIntroduction() {
		return "ffxiv-tools-app";
	}
	
	@Autowired
	private TaskExecutor executor;
	
	@GetMapping(path = "/api/heart-beats", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public SseEmitter getHeartBeats() {
		SseEmitter emitter = new SseEmitter();
		this.executor.execute(() -> {
			do {				
				try {
					emitter.send("I'm fine");
				} catch (IOException e) {
					return;
				}
				
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					return;
				}
			} while (true);
		});
		return emitter;
	}
	
	@Bean
	PlayerCharacterController playerCharacterController(
			BaseData baseData, ItemRepository itemRepo,
			PlayerCharacterService service) {
		return new PlayerCharacterController(service, itemRepo, baseData);
	}
	
	@Bean
	ActorController actorController() {
		return new ActorController(this.actorService());
	}
	
	@Bean
	ItemController itemController(
			ItemRepository itemRepo,
			ItemService itemService) {
		return new ItemController(itemService, itemRepo);
	}
	
	@Bean
	InventoryController inventoryController(InventoryService invService) {
		return new InventoryController(invService);
	}
	
	@Bean
	EquipmentUpgradeController equipmentUpgradeController(
			BaseData baseData, ItemRepository itemRepo,
			PlayerCharacterService pcService) {
		return EquipmentUpgradeController.builder()
				.baseData(baseData)
				.calculator(new GearCalculator(pcService, itemRepo, baseData))
				.pcService(pcService)
				.build();
	}
	
	@Bean
	CraftingController craftingController(
			InventoryService invService,
			ItemRecipeResolver recipeResolver,
			RecipeRepository recipeRepo) {
		return new CraftingController(
				new CraftingCalculator(recipeResolver, recipeRepo), 
				invService);
	}
	
	@Bean
	ItemService itemService(
			ItemRepository itemRepo,
			RecipeRepository recipeRepo) {
		return new ItemService(itemRepo, recipeRepo);
	}
	
	@Bean
	PlayerCharacterService playerCharacterService(
			BaseData baseData,
			PlayerCharacterRepository pcRepo) {
		return new PlayerCharacterService(baseData, pcRepo, this.actorService());
	}
	
	@Bean
	ActorService actorService() {
		return new ActorService();
	}
	
	@Bean
	PlayerCharacterRepository playerCharacterRepository(
			@Value("${ffxiv-tools.data-dir:data}") String dataDir) {
		return new PlayerCharacterDataRepository(dataDir);
	}
	
	@Bean
	InventoryService inventoryService(PlayerCharacterService pcService) {
		return new InventoryService(pcService);
	}
	
	@Bean
	ItemRepository itemRepo(BaseData baseData) {
		return new ItemRepository(new ItemParser(baseData).parseLatest());
	}
	
	@Bean
	RecipeRepository recipeRepository(
			BaseData baseData,
			ItemRepository itemRepo) {
		RecipeParserV5 recipeParser = new RecipeParserV5(baseData, 
				itemId -> itemRepo.getById(itemId).getName());
		return new RecipeRepository(recipeParser.parse(), itemRepo);
	}
	
	@Bean
	ItemRecipeResolver itemRecipeResolver(RecipeRepository recipeRepo) {
		return new ItemRecipeResolver(recipeRepo, new FreeCompanyRecipeRepository());
	}
	
	@Bean
	BaseData baseData(
			@Value("${ffxiv-tools.game-data.xivapi.cache-dir:}") String xivapiDataCacheDir) {
		BaseDataInitialization.Config config = new BaseDataInitialization.Config(xivapiDataCacheDir);
		return new BaseDataInitialization().init(config);
	}
	
	@Bean
	TaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor result = new ThreadPoolTaskExecutor();
		result.setMaxPoolSize(10);
		result.setQueueCapacity(20);
		return result;
	}
	
	@Bean
	WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {

			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/api/**");
			}
		};
	}
}
